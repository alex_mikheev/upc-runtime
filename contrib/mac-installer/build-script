#!/bin/sh
#
# TO USE:
# You need the README, install.command and uninstall.command that came with this script.
# You have two options for the sources:
# 1) Unpack the runtime and translator archives in the same directory as this script
# 2) Edit BUPC_URL_BASE setting below as needed to automatically fetch the sources.
# Then run with 1 argument: the Berkeley UPC release version
#   example: ./build-script 2.10.0
# Currently must be run in the directory where the script resides.
#
# This script orignally embodied the following instructions that previously lived in
# upcr/README.developers (plus updates, additional logic and configure flags):
#
#  - configure & build OSX runtime binaries, use CC="gcc -arch ppc" and CXX="g++ -
#    arch ppc" to ensure app code built on Intel Macs gets correct ABI to link against
#    binary libs
#  - Run a recursive grep to look for any instances of $HOME, or other system-specific
#    paths
#  - run full test harness on the runtime builds
#  - Install runtime: gmake install prefix=/tmp/###BININSTALL_PREFIX###
#    You must use exactly that prefix because the install script counts on it.
#  - Copy upc-examples directory into /tmp/###BININSTALL_PREFIX###
#  - remove extraneous files from upc-examples:
#     rm -f `find upc-examples -name harness.conf\*`
#     rm -Rf `find upc-examples -name CVS`
#  - Ensure appropriate permissions:
#     chmod -R a+rX,og-w /tmp/###BININSTALL_PREFIX###
#  - Create runtime tarball:
#    cd /tmp/###BININSTALL_PREFIX###
#    tar -cvf bupc_runtime.tar * ; gzip -9 bupc_runtime.tar
#    mv bupc_runtime.tar.gz bupc_runtime-archive
#  - build & install translator anywhere, then tar-gz the installed translator with
#    dirname "trans"
#    and name it bupc_translator-archive (note, we don't use .tar.gz to prevent bug1517)
#  - create full and lite installer tarballs -
#     Copy the install scripts from upcr/contrib/mac-installer
#     Update embedded version information in install.command appropriately for each installer
#   full installers contain install scripts, runtime archive and translator archive
#   lite installers are the same just without the translator archive
#

BUPC_URL_BASE=http://mantis.lbl.gov/nightly/upc

function size () {
  expr \( 1023 + `du -ks "$1" | cut -f1` \) / 1024
}

BUPC=$1
if [ -z "$BUPC" ]; then
  echo "Must provide BUPC version as first argument"
  exit 1;
fi

upcr_name="berkeley_upc-${BUPC}"
upcr_dir="`pwd`/${upcr_name}"
if [ ! -e "$upcr_dir" ]; then
  curl -LsS ${BUPC_URL_BASE}/${upcr_name}.tar.gz | tar xz
fi
if [ ! -e "$upcr_dir" ]; then
  echo "Runtime directory berkeley_upc-${BUPC} does not exist"
  exit 1;
fi
trans_name="berkeley_upc_translator-${BUPC}"
trans_dir="`pwd`/${trans_name}"
if [ ! -e "$trans_dir" ]; then
  curl -LsS ${BUPC_URL_BASE}/${trans_name}.tar.gz | tar xz
fi
if [ ! -e "$trans_dir" ]; then
  echo "Translator directory berkeley_upc_translator-${BUPC} does not exist"
  exit 1;
fi

full_darwin_version=`/usr/bin/uname -r 2>&1`

case `uname -p` in
*86*) 
 # 64-bit output on MacOS 10.7 and later (Darwin >= 11)
 case $full_darwin_version in
 [7-9].* | 10.*)  target=i686   ;;
              *)  target=x86_64 ;;
 esac
 ;;
*pc*) target=ppc ;;
*)
  echo "Couldn't guess the achitecture"
  exit 1
  ;;
esac

# Runtime should use same compiler as Xcode default:
CC="cc -arch $target"
CXX="c++ -arch $target"
export CC
export CXX

# Translator must use gcc (Clang not yet supported)
TRANS_CC="gcc -arch $target"
TRANS_CXX="g++ -arch $target"
export TRANS_CC
export TRANS_CXX

# If we used more than this PATH then the result would be non-portable
PATH=/usr/bin:/bin:/usr/sbin:/sbin
export PATH

# This is just the versioning used for naming the archives:
case $full_darwin_version in
7.*)   XCODE="1.x";;
8.*)   XCODE="2.2";;
9.*)   XCODE="3.0";;
10.*)  XCODE="3.2";;
11.*)  XCODE="4.1";;
12.*)  XCODE="5.0";;
*)
  echo "Couldn't guess the Xcode version for Darwin version $full_darwin_version"
  echo "Please update the build-script and the Xcode table in README.developers"
  exit 1
  ;;
esac

set -e
set -x

if [ -d DISTTMP ] ; then
  chmod -Rf u+w DISTTMP
  rm -rf DISTTMP
fi
mkdir DISTTMP
cd DISTTMP
TMPDIR=`pwd`

cd ${TMPDIR}
mkdir bld
cd bld
# Notes on configure flags:
# --enable-allow-gcc4
#    Because Apple's gcc versions are in the "buggy" range
# --enable-smp --enable-udp
#    Because we don't want to build an installer lacking either of these
# --disable-auto-conduit-detect --disable-mpi-compat
#    Because we don't want to build for mpi (or vapi on SystemX) that user might not have
# --enable-pthreads
#    Because we don't want to build if pthread detection fails
# --enable-smp-safe
#    Because we don't want to build a uni-processor binary if build host is uni-processor
# --disable-ppc64-probe
#    Because we don't want to use ppc64 instructions that user's host might not support
# gasnet_cv_datacache_line_size=128
#    Because this is our "safe default" w/o knowing the user's host machine
#
${upcr_dir}/configure --enable-allow-gcc4 --enable-smp --enable-udp --disable-auto-conduit-detect --disable-mpi-compat --enable-pthreads --enable-smp-safe --disable-ppc64-probe gasnet_cv_datacache_line_size=128
make all
make install DESTDIR=`pwd`/.. prefix='/tmp/###BININSTALL_PREFIX###'
cd '../tmp/###BININSTALL_PREFIX###'
cp -R ${upcr_dir}/upc-examples .
chmod -R u+w upc-examples
rm -f `find upc-examples -name harness.conf\*`
chmod -R a+rX,og-w .
upcrsz=`size .`
tar cf bupc_runtime.tar *
gzip -9v bupc_runtime.tar
mv bupc_runtime.tar.gz ${TMPDIR}/bupc_runtime-archive

cd ${TMPDIR}
cp -R ${trans_dir} compiler-build
chmod -R u+w compiler-build
cd compiler-build
make CC="$TRANS_CC" CXX="$TRANS_CXX"
make install PREFIX=`pwd`/trans CC="$TRANS_CC" CXX="$TRANS_CXX"
chmod -R a+rX,og-w trans
transsz=`size trans`
tar cf bupc_translator.tar trans
gzip -9v bupc_translator.tar
mv bupc_translator.tar.gz ${TMPDIR}/bupc_translator-archive

cd ${TMPDIR}/bld
env UPCC_NORC=1 UPCC_FLAGS=--translator=${TMPDIR}/compiler-build/trans/targ make check

cd ${TMPDIR}
cp ../{README,install.command,uninstall.command} .
chmod +w install.command uninstall.command
expect_darwin_version=`echo $full_darwin_version | cut -d. -f1`
expect_cc_version=`$CC -v 2>&1 | perl -ne 'if (/.*?([a-zA-Z]* version [0-9.]*).*/) {print $1;}'`
expect_hardware_version="`/usr/bin/uname -m 2>&1`"
perl -pi -e "s/^packageversion=.*/packageversion=\"$BUPC\"\nexpect_darwin_version=\"$expect_darwin_version\"\nexpect_cc_version=\"$expect_cc_version\"\nexpect_hardware_version=\"$expect_hardware_version\"/;" -- install.command
perl -pi -e "s/^packageversion=.*/packageversion=\"$BUPC\"/;" -- uninstall.command
chmod a+rX,og-w README install.command uninstall.command

DIR="berkeley_upc_${BUPC}_xcode${XCODE}_${target}"
mkdir $DIR
mv README install.command uninstall.command bupc_runtime-archive bupc_translator-archive $DIR/
tar cvf ../${DIR}.tar ${DIR}

rm ${DIR}/bupc_translator-archive
mv ${DIR} ${DIR}-lite
tar cvf ../${DIR}-lite.tar ${DIR}-lite

cd ..
gzip -9v ${DIR}.tar ${DIR}-lite.tar

chmod -Rf u+w DISTTMP
#rm -rf DISTTMP

cat <<_EOF
=========================================================================
Arguments:
      bupc_version = $BUPC

Settings:
     xcode_version = $XCODE
    darwin_version = $expect_darwin_version
        cc_version = $expect_cc_version
  hardware_version = $expect_hardware_version
                CC = '$CC'
               CXX = '$CXX'

Created full installer: ${DIR}.tar.gz
      Archive: `size ${DIR}.tar.gz` MB
    Installed: `expr $upcrsz + $transsz` MB
Created lite installer: ${DIR}-lite.tar.gz
      Archive: `size ${DIR}-lite.tar.gz` MB
    Installed: $upcrsz MB

  PLEASE, test:
  + Full harness runs in ${TMPDIR}/bld
  + 'sudo ./install.command' to install in the default location
    - Run the upcc and upcrun commands printed at the end of the install
    - Ensure manpages look right (including version number)
    - 'sudo ./uninstall.command' to remove
  + Repeat as non-root w/ a LOCALROOT=something argument to both scripts
  + Test on another compatible host if at all possible
=========================================================================
_EOF
