#!/bin/bash
# This is a helpful script to run on n2001.lbl.gov to build
# the final release tarballs from a GIT tag (or branch name).
# Note that by default it copies in the pre-built GASNet docs from
# a previous release - one can set env var GASNET_DOCS to use a
# local directory once they are finally revised.  -PHH 2014.04.18
TAG=$1
RC=$2
if test -z "$TAG" ; then
  echo "Missing required argument (a git tag or branch name)"
  exit 1
fi
TOPDIR=`pwd`
# Edit the following if the network translator should be tested instead
export UPCC_FLAGS="-translator=${TOPDIR}/trans-inst/targ"
export UPCC_NORC=1
#
set -e
set -x
#
.  /usr/local/pkg/Modules/init/bash
module unload autotools vapi gm gm2 mpi gmake gcc
module load autotools/autotools-newest
module load gmake/3.82
module load gcc/gcc-4.4.2
module load git
#
# mkdirs (to fail if not "clean")
#
mkdir bld-upcr
mkdir bld-gasnet
mkdir bld-tools
#
# Fetch docs from a previous version
#
if test -z "$GASNET_DOCS"; then
  DOCS_FROM=GASNet-1.16.0
  curl -LsS http://gasnet.lbl.gov/${DOCS_FROM}.tar.gz | \
    tar xz ${DOCS_FROM}/docs
  GASNET_DOCS=`pwd`/${DOCS_FROM}/docs
fi
#
# GIT checkouts
#
for repo in gasnet upc-runtime upc-translator; do
  #git clone --branch=$TAG https://bitbucket.org/berkeleylab/$repo
  git clone --branch=$TAG git@bitbucket.org:berkeleylab/$repo
done
cp -rp gasnet gasnet-tools
cp -rp gasnet upc-runtime/
##
## Function to extract version
##
function get_ver {
  local keyword="$1"
  local file=$2
  local version=$(perl -ne 'if (m/'"$keyword"'(\d+\.\d+\.\d+)/) { print "$1\n"; exit 0; }' -- $file)
  if test -z "$version" ; then
    set +x
    echo ERROR: Could not extract version info from $file >&2
    exit 1
  fi
  echo "$version"
}
##
## Function to relocate/repackage tarball as required
##
function do_pkg {
  local blddir=$1
  local pkg=$2
  if test -n "$RC"; then
    basepkg=`basename $pkg $RC`
    tar xfz ${blddir}/${basepkg}.tar.gz
    mv ${basepkg} ${pkg}
    GZIP=--best tar chfz ${pkg}.tar.gz ${pkg}
    rm -rf ${pkg} ${blddir}/${basepkg}.tar.gz
  else
    mv ${blddir}/${pkg}.tar.gz .
  fi
}
#
# Translator tarball
#
cd ${TOPDIR}/upc-translator
gmake dist
cd ${TOPDIR}
TRANS_PKG=berkeley_upc_translator-$(get_ver 'release ' upc-translator/open64/osprey1.0/be/whirl2c/w2c_driver.cxx)$RC
do_pkg upc-translator $TRANS_PKG
#
# Validate nodist-list
#
cd ${TOPDIR}/upc-translator
gmake update-nodist-list
if ! git diff --quiet -- nodist-list; then
  set +x
  echo ERROR: translator nodist-list is not up-to-date
  exit 1
fi
#
# Translator install from tarball
#
cd ${TOPDIR}
tar xfz ${TRANS_PKG}.tar.gz
make -C ${TRANS_PKG} all
make -C ${TRANS_PKG} install PREFIX=${TOPDIR}/trans-inst
rm -rf ${TRANS_PKG}
#
# Check unBootstrap
#
cd ${TOPDIR}/upc-runtime
./Bootstrap -L
./unBootstrap
for dir in ${TOPDIR}/upc-runtime ${TOPDIR}/upc-runtime/gasnet; do
  cd $dir
  if test 0 -ne `git status --ignored --porcelain | grep -v 'gasnet/$' | wc -l`; then
    set +x
    echo ERROR: Found untracked files after unBootstrap in `basename $dir`:
    git status --ignored --short | grep -v 'gasnet/$'
    exit 1
  fi
done
#
# Check that scanner.c is not older than scanner.l
# TODO: this probably belongs in the dist-hook instead
#
cd ${TOPDIR}/upc-runtime
if test $(git log -n1 --format='%ct'  -- detect-upc/scanner.c) -lt \
        $(git log -n1 --format='%ct'  -- detect-upc/scanner.l); then
  set +x
  echo ERROR: upc-runtime/detect-upc/scanner.c is older than scanner.l
  exit 1
fi
#
# Runtime tarball
#
cd ${TOPDIR}/upc-runtime
./Bootstrap -L
cd ${TOPDIR}/bld-upcr
../upc-runtime/configure --enable-totalview --without-multiconf
gmake distcheck GASNET_DOCS=$GASNET_DOCS
cd ${TOPDIR}
UPCR_PKG=berkeley_upc-$(get_ver 'VERSION=' upc-runtime/configure)$RC
do_pkg bld-upcr $UPCR_PKG
#
# GASNet tarball
#
cd ${TOPDIR}/gasnet
./Bootstrap
cd ${TOPDIR}/bld-gasnet
../gasnet/configure
gmake distcheck GASNET_DOCS=$GASNET_DOCS
cd ${TOPDIR}
GASNET_PKG=GASNet-$(get_ver 'VERSION=' gasnet/configure)$RC
do_pkg bld-gasnet $GASNET_PKG
#
# Tools tarball
#
cd ${TOPDIR}/gasnet-tools
./Bootstrap -o
cd ${TOPDIR}/bld-tools
../gasnet-tools/configure
gmake distcheck GASNET_DOCS=$GASNET_DOCS
cd ${TOPDIR}
TOOLS_PKG=GASNet_Tools-$(get_ver 'VERSION=' gasnet-tools/configure)$RC
do_pkg bld-tools $TOOLS_PKG
#
# Report versions
#
set +x
echo 'Reported git versions:'
for pkg in ${TRANS_PKG} ${UPCR_PKG} ${GASNET_PKG} ${TOOLS_PKG}; do
    echo -ne "$pkg\n\t"
    tar xOfz ${pkg}.tar.gz ${pkg}/version.git
done
