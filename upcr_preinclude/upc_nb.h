/* Reference implementation of upc_nb.h 
 * Copyright 2012, The Regents of the University of California 
 * This code is under BSD license: http://upc.lbl.gov/download/dist/LICENSE.TXT
 */

#ifndef _UPC_NB_H
#define _UPC_NB_H

#if !defined(__BERKELEY_UPC_FIRST_PREPROCESS__) && !defined(__BERKELEY_UPC_ONLY_PREPROCESS__)
#error This file should only be included during initial preprocess
#endif

#if __UPC_NB__ != 1
#error Bad feature macro predefinition
#endif

#include <bupc_extensions.h>

#undef  UPC_COMPLETE_HANDLE
#define UPC_COMPLETE_HANDLE  BUPC_COMPLETE_HANDLE
#undef  upc_handle_t
#define upc_handle_t         bupc_handle_t

#define upc_memcpy_nb        bupc_memcpy_async
#define upc_memget_nb        bupc_memget_async
#define upc_memput_nb        bupc_memput_async
#define upc_memset_nb        bupc_memset_async

#define upc_memcpy_nbi       bupc_memcpy_asynci
#define upc_memget_nbi       bupc_memget_asynci
#define upc_memput_nbi       bupc_memput_asynci
#define upc_memset_nbi       bupc_memset_asynci

#define upc_sync_attempt     bupc_trysync
#define upc_sync             bupc_waitsync
#define upc_synci_attempt    bupc_trysynci
#define upc_synci            bupc_waitsynci

#endif /* _UPC_NB_H */
