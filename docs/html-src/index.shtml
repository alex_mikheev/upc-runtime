<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#set var="VERSION" value="@VERSION@" -->
<!--#set var="TOPDIR" value="../download/berkeley_upc-@VERSION@" -->
<title>Berkeley UPC Documentation, v<!--#echo var="VERSION"--></title>
</head>

<body bgcolor="#ffffff">

<!--#include virtual="/header.html" -->

<table border=0 cellpadding=0 cellspacing=0 width=100%><tr><td>
<h1>Berkeley UPC Documentation <small>version <!--#echo var="VERSION"--></small></h1>
</td><td align=right>
<!-- SiteSearch Google -->
<FORM method=GET action="http://www.google.com/search">
<input type=hidden name=ie value=UTF-8>
<input type=hidden name=oe value=UTF-8>
<TABLE bgcolor="#FFFFFF" border=0 cellpadding=0 cellspacing=0><tr><td>
<!---
<A HREF="http://www.google.com/">
<IMG SRC="http://www.google.com/logos/Logo_40wht.gif" border="0" ALT="Google"></A>
-->
</td> <td>
<INPUT TYPE=text name=q size=31 maxlength=255 value="">
<INPUT type=submit name=btnG VALUE="Google Search">
<input type=hidden name=domains value="http://upc.lbl.gov"><br>
<table border=0 cellpadding=0 cellspacing=0><tr><td valign=top>
<font size=-1>
<input type=radio name=sitesearch value="http://upc.lbl.gov/docs" checked> Berkeley UPC Documentation <br>
<input type=radio name=sitesearch value="http://upc.lbl.gov/"> Berkeley UPC Site-wide <br>
</font>
</td><td valign=top>
<font size=-1>
<input type=radio name=sitesearch value=""> Entire Web
</font>
</td></tr></TABLE>
</td></tr>
<tr><td></td><td>
<!--#include virtual="../doc-versions.shtml"-->
</td></tr>
</table>
</FORM>
<!-- SiteSearch Google -->
</td></tr></table>

<h2>Installing Berkeley UPC</h2>

System requirements for using Berkeley UPC can be found on our
<a href="http://upc.lbl.gov/download/">download page</a>.<br>
Installation and configuration instructions, and release notes are contained in the download files, or here:
<ul>
    <li><a href="<!--#echo var="TOPDIR"-->/INSTALL.TXT">INSTALL.TXT</a>
    <li><a href="<!--#echo var="TOPDIR"-->/docs/README.crosscompile">README.crosscompile</a>
    <li>ChangeLog files for <a href="<!--#echo var="TOPDIR"-->/ChangeLog">Berkeley UPC</a>
                        and <a href="<!--#echo var="TOPDIR"-->/gasnet/ChangeLog">GASNet</a>
</ul>

<h2>Berkeley UPC Specific Documentation</h2>

<ul>
    <li><b><a href="user/index.shtml">Berkeley UPC User's Guide</a></b> - using the compiler, Berkeley extensions, and known limitations
    <li><a href="user/upcc.html">upcc man page</a>
    <li><a href="user/upcrun.html">upcrun man page</a>
    <li><a href="user/upc_trace.html">upc_trace man page</a>
    <li><a href="user/upcdecl.html">upcdecl man page</a>
    <li><a href="user/interoperability.shtml">Mixing C/C++/MPI/FORTRAN with UPC</a>. 
    <li><a href="user/sshagent.shtml">ssh-agent tutorial</a> (for
    installations using a remote translator over ssh)
    <li><a href="http://upc.lbl.gov/upcdecl">upcdecl online tool</a> -
	convert UPC declarations to English and back
</ul>

<h2>Language Documentation</h2>

<ul>
<li><a href="http://upc.lbl.gov/publications/upc-spec-1.3.pdf">UPC Language and Library Specifications, Version 1.3</a> - 
  Complete UPC 1.3 specification
<br>Also available in three parts, by section:
<ul>
<li><a href="user/upc-lang-spec-1.3.pdf">UPC Language Specifications, Version 1.3</a> -
    The base language and core <tt>&lt;upc.h&gt;</tt> libraries
<li><a href="user/upc-lib-required-spec-1.3.pdf">UPC Required Library Specifications, Version 1.3</a> -
    Collectives and Wall-Clock Timer libraries
<li><a href="user/upc-lib-optional-spec-1.3.pdf">UPC Optional Library Specifications, Version 1.3</a> -
    Atomics, Castability, Parallel I/O and Non-Blocking Transfer libraries
</ul>
</li>

<li><a href="http://www.iso.org/iso/en/CatalogueDetailPage.CatalogueDetail?CSNUMBER=29237&ICS1=35&ICS2=60&ICS3=">ISO-C 99 Standard</a> (upon which UPC is based) - ISO/IEC 9899:1999
<li> UPC Language Tutorials: &nbsp;
<a href="http://upc.lbl.gov/publications/#tutorial">Berkeley UPC Tutorials</a>, &nbsp;
<a href="http://www.gwu.edu/~upc/tutorials.html">UPC Community Tutorials</a>
<li> <a href="http://upc-lang.org">UPC Language Community website</a>
<li> <a href="http://upc.lbl.gov/publications/UPC-TR-Original99.pdf">Introduction to UPC and Language Specification</a> William W. Carlson et al., LLNL, CCS-TR-99-157, May 13, 1999.
</ul>

<h2>Design and Implementation Documentation</h2>

<a href="system/index.shtml">System design/implementation documentation</a> is included in
the release.

<p>A number of <a href="http://upc.lbl.gov/publications/">publications and
    presentations</a> resulting from the Berkeley UPC project are available on
our website.

<p>&nbsp;

<!-- don't touch stuff below this line -->
<hr>
<!--#include virtual="/footer.html"-->
<p>This page last modified on <!--#flastmod file="$DOCUMENT_NAME"--></p>
</body>
</html>
