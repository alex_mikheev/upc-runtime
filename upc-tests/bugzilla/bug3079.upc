#include <upc.h>
#include <stdio.h>

int main(void) {
  shared int *A = upc_all_alloc(THREADS*THREADS, sizeof(int));
  shared int *p, *q;
  int i,th;

  for (th=0; th<THREADS; ++th) {
    if (th == MYTHREAD) { // Orderly output
      int *s = malloc(sizeof(int));
      p = bupc_inverse_cast(s);
      free(s);
      printf("th%d sees private %p p=(%d,%p) %s\n", th, s,
             (int)upc_threadof(p), (void *)(uintptr_t)upc_addrfield(p), 
             (p==NULL)?"PASS":"FAIL");

      for (i=0; i<THREADS; ++i) {
        q = &A[i*(THREADS+1)];
        if (bupc_castable(q)) {
          p = bupc_inverse_cast(bupc_cast(q));
          printf("th%d sees A[%d]=(%d,%p) p=(%d,%p) %s\n", MYTHREAD, i*(THREADS+1),
                 (int)upc_threadof(q), (void *)(uintptr_t)upc_addrfield(q),
                 (int)upc_threadof(p), (void *)(uintptr_t)upc_addrfield(p),
                 (p==q)?"PASS":"FAIL");
        }
      }
      fflush(stdout);
    }
    upc_barrier;
  }

  if (!MYTHREAD) puts("done.");

  return 0;
}
