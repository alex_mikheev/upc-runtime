/* UPC [*] qualifier may not be used in declaration of pointers.  */

#include <upc.h>

shared [*] int *pts;
