/* Field declared with UPC shared qualifier.  */

#include <upc.h>

struct S_struct
  {
    int field1;
    shared double field2;
    char field3;
  };
