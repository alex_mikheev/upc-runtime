#!/bin/sh
# This is a simple script to find runlists are not yet completed.
# Optional arguments indicate direcrories to search, default is '.'
# Flag '-q' will list the qscript(s) instead of the runlist(s)
if [ x"$1" = 'x-q' ] ; then
  shift;
  dash_q=1
else
  dash_q=0
fi
find ${*:-.} -name 'runlist_*' -print |\
  grep -E 'runlist_[0-9]+_[0-9]+$' |\
  while read x; do
    if [ -e $x"-complete" -o -z "$x" ] ; then
      : # nothing
    elif [ $dash_q = 0 ]; then
      echo $x
    else
      d=`dirname $x`
      f=`basename $x`
      grep -l '\<'$f'\>' $d/qscript_*
    fi
  done
