# Compiler spec file for Intrepid GCC/UPC 4.x, stand-alone version

# upc compiler command
upc_compiler = /usr/local/clang-upc/bin/clangupc -g -O2 -I. -Wno-deprecated -fno-caret-diagnostics -Wno-logical-op-parentheses -Wno-unused-comparison -Wno-unused-value -Wno-dangling-else -Wno-duplicate-decl-specifier
dash_g = -g
dash_O = -O3

# upc run command
# Following replacements are performed:
# %N - number of UPC threads
# %P - program executable
# %A - arguments to program
# %B - berkeley-specific upcrun arguments (should appear if and only if this is Berkeley upcrun)
upcrun_command = %P -fupc-threads-%N %A

# default sysconf file to use
# -network setting is ignored for non-UPCR compilers, 
# just need a sysconf with a relevant batch policy for job execution
default_sysconf = shmem-interactive

# list of supported compiler features
feature_list = gupc,upc_all_free,upc_collective,upc_tick,os_linux,cpu_x86_64,cpu_64,cc_gnu

# option to pass upc compiler to get %T static threads
upc_static_threads_option = -fupc-threads-%T

# option for performing just the source-to-source compile step
# or empty if not supported by the compiler
upc_trans_option = 

# colon-delimited path where to find harness.conf files
suite_path = $TOP_SRCDIR$/upc-tests:$TOP_SRCDIR$/upc-examples

# GNU make
gmake = make

# misc system tools
ar = ar
ranlib = ranlib

# C compiler & flags (should be empty on upcr/GASNet to auto-detect)
cc = gcc
cflags =
ldflags = 
libs =

# host C compiler (or empty for same as cc)
host_cc = 
host_cflags =
host_ldflags =
host_libs = 

# known failures, in the format: test-path/test-name[failure comment] | test-name[failure comment]
# lines may be broken using backslash
# known failures may also include more specific failure/platform selectors, eg:
# mupc/test_stress_05-int [compile-all ; (cpu_ia64 || cpu_i686) && os_linux ; Compile failure on Itanium+x86 Linux... ] 
# known_failures may be empty to use the ones in the harness.conf files
known_failures = \
bug6b [compile-warning ; ; expect fail but got warning due to large value of UPC_MAX_BLOCK_SIZE] | \
bug247 [compile-failure ; cpu_32 ; Clang UPC does not support huge pointer offsets on 32-bit targets] | \
bug317a [compile-failure ; ; Clang UPC does not support casts from shared pointers to integers] | \
bug342 [compile-warning ; cpu_32 ; Clang UPC does not suppress the warning about __transparent_union__] | \
bug438 [compile-failure ; ; Clang UPC generates undefined external reference to an inlined function when compiled at -O0] | \
bug547 [compile-warning ; ; Clang UPC detects constants that exceed range of 'float'] | \
bug846 [compile-failure ; cpu_32 ; Clang UPC does not support the declaration of huge arrays on 32-bit targets] | \
bug3051 [compile-failure ; ; BUPC bug report 3051 - Zero sizeof(*p) for pointer to a UPC shared VLA] | \
bug3056 [compile-failure ; ; BUPC bug report 3056 - error when processing sizeof pointer to a UPC shared VLA] | \
bug3057 [ !compile-failure ; ; Clang UPC does not issue an error when compiling a PTS that refers to a multi-dim shared array with THREADS present in more than one dimension] | \
bugzilla/tricky_sizeof7 [ !compile-failure ; ; Clang UPC permits sizeof (void)] | \
gasnet-tests/testconduitspecific [compile-failure ; ;  Compile-time warning due to gcc 4.5.1 work-around] | \
gasnet-tests/testgasnet-parsync [compile-failure ; ; Missing gasnet-udp-parsync library, Makefile problem] | \
guts_main/barrier_neg [run-crash ; ; test intended to FAIL, but harness detects as CRASH] | \
gwu-npb-upc/btio-A [compile-failure ; packedsptr || cpu_32 ; Clang UPC does not support block sizes > 65536 in some configurations] | \
gwu-npb-upc/btio-A [run-match ; ; Bug 1508 - btio fails with NaNs] | \
gwu-npb-upc/btio-S [run-match ; ; Bug 1508 - btio fails with NaNs] | \
gwu-npb-upc/btio-W [compile-failure ; packedsptr || cpu_32 ; Clang UPC does not support block sizes > 65536 in some configurations] | \
gwu-npb-upc/btio-W [run-match ; ; Bug 1508 - btio fails with NaNs] | \
gwu-npb-upc/cg-A [run-match ; cpu_32 ; bug 653 - NPBs known to fail verification due to application bugs] | \
gwu-npb-upc/cg-S [run-match ; cpu_32 ; bug 653 - NPBs known to fail verification due to application bugs] | \
gwu-npb-upc/cg-W [run-match ; cpu_32 ; bug 653 - NPBs known to fail verification due to application bugs] | \
gwu-npb-upc/mg-S [run-match ; ; bug 653 - NPBs known to fail verification: mg-S with thread count >= 16] | \
perverse naming/p2 [compile-failure ; ; Known to fail when run more than once, due to bug in Makefile] | \
upc-semantic-checks/assign-pts-with-diff-block-factors-no-cast [compile-warning ; ; Clang UPC issues warning instead of error] | \
upc-semantic-checks/invalid-local-ptr-to-void-arith [ !compile-failure ; ; Clang UPC quietly allows arithmetic on local pointers-to-void] | \
upc-semantic-checks/invalid-sizeof-shared-void [ !compile-failure ; ; Clang UPC permits sizeof (shared void)] | \
upc-semantic-checks/invalid-sizeof-void [ !compile-failure ; ; Clang UPC permits sizeof (void)] | \
spec1.3/issue11 [compile-failure ;; Clang UPC does not allow casts from a pointer-to-shared to an integer] | \
spec1.3/issue64b [compile-failure ;; ICE: Clang UPC aborts on attempt to convert a PTS for argument to upc_barrier/upc_notify/upc_wait statement] | \
spec1.3/issue71 [compile-pass ; static ; Clang UPC does not diagnose [*] in typedefs as an error for static threads] | \
spec1.3/issue104 [compile-pass ;; Clang UPC does not diagnose certain relational operations involving a PTS as an error] | \
spec1.3/issue104a [compile-pass ;; Clang UPC does not diagnose certain relational operations involving a PTS as an error]
